# e3-autosave

Wrapper for the `autosave` module - part of EPICS infrastructure at ESS. This module is loaded through `essioc` for production IOCs.

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh -r "autosave"
```

### How it works

To set up autosave (and boot-time restore) of record fields, use one of the following info-tags:

* `autosaveFields`: to restore **before** and **after** record initialization
  - `settings.sav`: in `$(AS_TOP)/$(IOCNAME)/save`
  - `settings.req`: in `$(AS_TOP)/$(IOCNAME)/req`
 
* `autosaveFields_pass0`: to restore only **before** record initialization
  - `values_pass0.sav`: in `$(AS_TOP)/$(IOCNAME)/save`
  - `values_pass0.req`: in `$(AS_TOP)/$(IOCNAME)/req`

* `autosaveFields_pass1`: to restore only **after** record initialization
  - `values_pass1.sav`: in `$(AS_TOP)/$(IOCNAME)/save`
  - `values_pass1.req`: in `$(AS_TOP)/$(IOCNAME)/req`

For example:

```
record("*", "some-record") {
    info(autosaveFields, "PREC SCAN DESC OUT")
    info(autosaveFields_pass0, "VAL")
    info(autosaveFields_pass1, "VAL")
}
```

Each info tag should be matched with what one would like to use such as

## Contributing

Contributions through pull/merge requests only.
