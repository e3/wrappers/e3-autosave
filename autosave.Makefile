#  Copyright (c) 2017 - 2019   Jeong Han Lee
#  Copyright (c) 2019 - Current  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include ${E3_REQUIRE_TOOLS}/driver.makefile

USR_CFLAGS += -DDBLOADRECORDSHOOKREGISTER
USR_CFLAGS += -DUSE_TYPED_RSET

USR_CFLAGS += -Wno-unused-variable
USR_CFLAGS += -Wno-unused-function
USR_CPPFLAGS += -Wno-unused-variable
USR_CPPFLAGS += -Wno-unused-function

ASAPP := asApp
ASAPPDB := $(ASAPP)/Db
ASAPPSRC := $(ASAPP)/src

HEADERS += $(ASAPPSRC)/os/Linux/osdNfs.h

# OS independent code
SOURCES += $(ASAPPSRC)/dbrestore.c
SOURCES += $(ASAPPSRC)/save_restore.c
SOURCES += $(ASAPPSRC)/initHooks.c
SOURCES += $(ASAPPSRC)/fGetDateStr.c
SOURCES += $(ASAPPSRC)/configMenuSub.c
SOURCES += $(ASAPPSRC)/verify.c

# OS dependent code
SOURCES += $(ASAPPSRC)/os/Linux/osdNfs.c

DBDS += $(ASAPPSRC)/asSupport.dbd

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

TEMPLATES += ../template/save_restoreStatus.db
TEMPLATES += $(ASAPPDB)/configMenu.db

TEMPLATES += $(ASAPPDB)/configMenuNames.req
TEMPLATES += $(ASAPPDB)/configMenu.req
TEMPLATES += $(ASAPPDB)/configMenu_settings.req

USR_INCLUDES += -I$(COMMON_DIR)

asVerify.o: $(COMMON_DIR)/autosave_release.h
$(COMMON_DIR)/autosave_release.h:
	$(PERL) $(where_am_I)$(ASAPPSRC)/autosave_release.pl $(AUTOSAVE_RELEASE) > $@

########################################################
#
# Special code for asVerify
#
########################################################

ASVERIFY_SOURCES += verify.c
ASVERIFY_SOURCES += asVerify.c

# asVerify will be installed in $(E3_SITEMODS_PATH)/autosave/$(E3_MODULE_VERSION)/bin/asVerify
TEMP_PATH :=$(where_am_I)O.$(EPICSVERSION)_$(T_A)
ASVERIFY  :=$(TEMP_PATH)/bin/asVerify

BINS += $(ASVERIFY)

$(foreach s,$(ASVERIFY_SOURCES),$(eval vpath $s $(where_am_I)$(ASAPPSRC)))

# This is similar to what is done for e3-sequencer. See the comment here:
#
# https://gitlab.esss.lu.se/e3/wrappers/core/e3-sequencer/-/blob/49499146/sequencer.Makefile#L95-L109
#
# for more information.
$(DEPFILE): $(ASVERIFY)

$(ASVERIFY): $(ASVERIFY_SOURCES:.c=.o)
	@echo ""
	@echo ">>>>> asVerify Init "
	$(RM) $(ASVERIFY)
	$(MKDIR) -p $(TEMP_PATH)/bin
	$(CCC) -o $@ -L$(EPICS_BASE_LIB) -Wl,-rpath,$(EPICS_BASE_LIB) $(OP_SYS_LDFLAGS) $(ARCH_DEP_LDFLAGS) $(CODE_LDFLAGS) $^ -lca -lCom 
	@echo "<<<<< asVerify Done"
	@echo ""
